package luzzu.kurrent.engine.json

import scala.language.implicitConversions
import io.circe.Decoder
import luzzu.kurrent.engine.ImageData
import luzzu.kurrent.json.JsonDecoder

object ImageDataDecoder {

  implicit val imageDataDecoder: Decoder[ImageData] = JsonDecoder.custom { cursor =>
    for {
      id          <- cursor.downField("Id").as[String]
      parentId    <- cursor.downField("ParentId").as[String]
      repoDigests <- cursor.downField("RepoDigests").as[Option[Seq[String]]]
      repoTags    <- cursor.downField("RepoTags").as[Seq[String]]
      created     <- cursor.downField("Created").as[Long]
      size        <- cursor.downField("Size").as[Long]
      virtualSize <- cursor.downField("VirtualSize").as[Long]
      sharedSize  <- cursor.downField("SharedSize").as[Long]
      labels      <- cursor.downField("Labels").as[Option[Map[String, String]]]
      containers  <- cursor.downField("Containers").as[Int]
    } yield ImageData(
      id          = id,
      parentId    = parentId,
      repoDigests = repoDigests getOrElse Seq.empty,
      repoTags    = repoTags,
      created     = created,
      size        = size,
      virtualSize = virtualSize,
      sharedSize  = sharedSize,
      labels      = labels getOrElse Map.empty,
      containers  = containers
    )
  }

}
