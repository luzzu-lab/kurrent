package luzzu.kurrent.engine.json

import scala.language.implicitConversions
import io.circe.Decoder
import luzzu.kurrent.engine.ErrorData
import luzzu.kurrent.json.JsonDecoder

object ErrorDataDecoder {

  implicit val errorDataDecoder: Decoder[ErrorData] = JsonDecoder.custom {
    _.downField("message").as[String].map { ErrorData }
  }

}
