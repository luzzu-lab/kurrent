package luzzu.kurrent.engine

case class ImageData(id: String,
                     parentId: String,
                     repoDigests: Seq[String],
                     repoTags: Seq[String],
                     created: Long,
                     size: Long,
                     virtualSize: Long,
                     sharedSize: Long,
                     labels: Map[String, String],
                     containers: Int)
