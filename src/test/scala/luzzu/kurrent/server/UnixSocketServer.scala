package luzzu.kurrent.server

import java.nio.charset.Charset
import java.nio.channels.SelectionKey
import java.nio.file.Files

import scala.collection.JavaConverters._

import cats.effect.IO
import cats.syntax.traverse.toTraverseOps
import cats.instances.list.catsStdInstancesForList

import jnr.enxio.channels.NativeSelectorProvider
import jnr.unixsocket.{ UnixServerSocketChannel, UnixSocketAddress }

sealed class UnixSocketServer(val path: String, private val clientGen: ClientGen)(implicit encoding: Charset = Charset.defaultCharset) extends Runnable {

  private val address = new UnixSocketAddress(path)

  private val channel = UnixServerSocketChannel.open()

  private val selector = NativeSelectorProvider.getInstance().openSelector().wakeup()

  channel.configureBlocking(false)
  channel.socket().bind(address)
  channel.register(selector, SelectionKey.OP_ACCEPT, new AcceptActor(channel, selector, clientGen))

  private def selectKeys() = IO {
    selector.selectedKeys().iterator().asScala.map { _.attachment() }.collect {
      case server: ServerActor => Left(server)
      case client: ClientActor => Right(client)
    }.toList
  }

  private def runActors(actors: List[Either[ServerActor, ClientActor]]) = actors.sortBy {
    _.isLeft
  }.traverse[IO, Unit] {
    case Left(server)                    => server.serve()
    case Right(client) if client.isReady => doNothing
    case Right(client)                   => client.act()

  }

  def close() = IO { channel.close() }

  private def runIO() = for {
    data <- selectKeys()
    _    <- runActors(data)
  } yield ()

  def run() = while (true) {
    if (selector.select() > 0) runIO().unsafeRunSync()
  }

  def create() = new Thread(this)

}

object UnixSocketServer {

  val defaultPath = Files.createTempDirectory("ipc-test").resolve(s"test.sock").toString

  def init(clientGen: ClientGen, path: String = defaultPath)(implicit encoding: Charset = Charset.defaultCharset) = new UnixSocketServer(path, clientGen)

  def echo(path: String = defaultPath)(implicit encoding: Charset = Charset.defaultCharset) = init(new EchoClientActor(_), path)

}
