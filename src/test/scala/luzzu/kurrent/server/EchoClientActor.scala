package luzzu.kurrent.server

import java.nio.ByteBuffer

import cats.effect.IO
import cats.syntax.traverse.toTraverseOps
import cats.instances.list.catsStdInstancesForList

import jnr.unixsocket.UnixSocketChannel

import luzzu.kurrent.Binary

class EchoClientActor(channel: UnixSocketChannel) extends ClientActor {

  private var ready: Boolean = false

  private def readChunks(binary: Binary = Binary.empty,
                         buffer: ByteBuffer = ByteBuffer.allocate(1024),
                         emptyCount: Int = 0): IO[Binary] = channel.read(buffer.clear()) match {
    case 0 if emptyCount < 3 => readChunks(binary, buffer, emptyCount + 1)
    case 0                   => cleanse { binary }
    case -1                  => cleanse { buffer.array ++ binary }
    case _                   => readChunks(buffer.array ++ binary, buffer, emptyCount)
  }

  private def writeChunk(chunk: Binary, buffer: ByteBuffer) = IO {
    channel.write { buffer.put(chunk).flip() }
  }

  private def cleanse(binary: Binary) = IO {
    binary.takeWhile { _ > 0  }
  }

  private def writeChunks(binary: Binary, buffer: ByteBuffer = ByteBuffer.allocate(1024)) = binary.grouped(buffer.capacity).toList.traverse[IO, Int] {
    writeChunk(_, buffer.clear())
  }.map { _.sum }

  private def readDirect(buffer: ByteBuffer = ByteBuffer.allocate(1024)) = IO {
    channel.read(buffer)
  }

  private def writeDirect(buffer: ByteBuffer) = IO {
    channel.write(buffer)
  }

  private def closeChannel() = IO { channel.close() }

  private def setReady(value: Boolean) = IO { ready = value }

  override def act() = for {
    binary <- readChunks()
    _      <- writeChunks(binary)
    _      <- closeChannel()
    _      <- setReady { true }
  } yield ()

  def isReady = ready

}
