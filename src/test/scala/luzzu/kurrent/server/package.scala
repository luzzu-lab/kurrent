package luzzu.kurrent

import cats.effect.IO

import jnr.unixsocket.UnixSocketChannel

package object server {

  type ClientGen = UnixSocketChannel => ClientActor

  def doNothing: IO[Unit] = IO()

}
