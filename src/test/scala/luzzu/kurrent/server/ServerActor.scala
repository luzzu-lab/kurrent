package luzzu.kurrent.server

import cats.effect.IO

trait ServerActor {

  def serve(): IO[Unit]

}

trait ClientActor {

  def isReady: Boolean

  def act(): IO[Unit]

}
