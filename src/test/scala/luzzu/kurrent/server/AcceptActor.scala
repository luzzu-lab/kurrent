package luzzu.kurrent.server

import java.nio.channels.{SelectionKey, Selector}

import cats.effect.IO
import jnr.unixsocket.{UnixServerSocketChannel, UnixSocketChannel}

class AcceptActor(channel: UnixServerSocketChannel, selector: Selector, clientGen: ClientGen) extends ServerActor {

  private def accept() = IO { channel.accept() }

  private def doConfigure(clientChannel: UnixSocketChannel) = IO {
    clientChannel.configureBlocking(false)
    clientChannel.register(selector, SelectionKey.OP_READ, clientGen(clientChannel))
  }

  private def configure(clientChannel: UnixSocketChannel) = if (clientChannel == null) doNothing else doConfigure(clientChannel)

  def serve() = for {
    actorChannel <- accept()
    _            <- configure(actorChannel)
  } yield ()

}
