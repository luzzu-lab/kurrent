package luzzu.kurrent.http

import java.time.ZonedDateTime

import org.scalatest.{ FlatSpec, Matchers }

import io.circe.Encoder

import luzzu.kurrent.Binary._
import luzzu.kurrent.{ Entity, NoEntity }
import luzzu.kurrent.api.http._
import luzzu.kurrent.api._
import luzzu.kurrent.json.TestContainer
import luzzu.kurrent.json.TestContainer._

class HttpResponseDecoderSpec extends FlatSpec with Matchers {

  "An Http response decoder" should "correctly decode a response without body " in {
    unixHttpResponseDecoder[NoEntity].decode {
      HttpResponseDecoderSpec.okString.binary
    } should be { HttpResponseDecoderSpec.ok }
  }

  "An Http response decoder" should "correctly decode a response with body " in {
    unixHttpResponseDecoder[HttpResourceEntity[TestContainer]].decode {
      HttpResponseDecoderSpec.okString(TestContainer("string", 128)).binary
    } should be { HttpResponseDecoderSpec.ok(TestContainer("string", 128)) }
  }

}

object HttpResponseDecoderSpec {

  private val date = ZonedDateTime.now()

  val headers = Set(
    HttpHeader.host,
    HttpHeader.date(date),
    HttpHeader.connection,
    HttpHeader.server("ScalaTest - Kurrent")
  )

  def responseString(statusCode: Int, statusMessage: String) = HttpString.inLine(s"HTTP/1.1 $statusCode $statusMessage", headers)

  def responseString[A](statusCode: Int, statusMessage: String, resource: A)(implicit E: Encoder[A]) = HttpString.inLine(
    s"HTTP/1.1 $statusCode $statusMessage",
    headers,
    E(resource).noSpaces
  )

  def responseHead(statusCode: Int, statusMessage: String) = UnixHttpResponseHead("1.1", statusCode, statusMessage)

  def response[A](statusCode: Int, statusMessage: String, resource: Option[A]): UnixHttpResponse[Entity] = resource match {
    case Some(a) => UnixHttpResponse(statusCode, statusMessage, "1.1", headers, HttpResourceEntity(a))
    case None    => UnixHttpResponse(statusCode, statusMessage, "1.1", headers, NoEntity)
  }

  def okString = responseString(200, "OK")

  def okString[A](resource: A)(implicit E: Encoder[A]) = responseString(200, "OK", resource)

  def ok[A](body: A) = response(200, "OK", Some(body))

  def ok = response(200, "OK", None)

}
