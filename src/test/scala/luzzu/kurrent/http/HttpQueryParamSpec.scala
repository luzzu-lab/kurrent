package luzzu.kurrent.http

import org.scalatest.{ FlatSpec, Matchers }

import HttpQueryParams.HttpQuerySyntax

class HttpQueryParamSpec extends FlatSpec with Matchers {

  "HttpQueryParam syntax" should "allow postfix binding" in {
    { "key" := "value" } should be { HttpQueryParam("key", "value") }
  }

  "HttpQueryParam apply" should "construct an instance from a string" in {
    HttpQueryParam { "key=value" } should be { Some("key" := "value") }
  }

  "HttpQueryParam" should "unapply using :=" in {
    "key=value" match {
      case k := v =>
        k should be { "key" }
        v should be { "value" }
    }
  }

}
