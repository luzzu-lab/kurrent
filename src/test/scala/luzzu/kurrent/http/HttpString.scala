package luzzu.kurrent.http

object HttpString {

  def inLine(head: String, headers: Set[HttpHeader], body: String = "") =
  { head +: headers.map { case key :> value => s"$key: $value" }.toSeq :+ body :+ "\n\r" }.mkString("\n")

}
