package luzzu.kurrent.http

import java.time.ZonedDateTime

import org.scalatest.{ FlatSpec, Matchers }

import io.circe.Encoder

import luzzu.kurrent.Binary._
import luzzu.kurrent.{ Entity, NoEntity }
import luzzu.kurrent.api.http._
import luzzu.kurrent.api._
import luzzu.kurrent.json.TestContainer
import luzzu.kurrent.json.TestContainer._

class HttpRequestEncoderSpec extends FlatSpec with Matchers {

  "An Http request encoder" should "correctly encode a GET request without body " in {
    unixHttpRequestEncoder[NoEntity].encode {
      UnixHttpRequest.GET("/test/no-entity").withHeaders { HttpRequestEncoderSpec.headers }
    }.string should be { HttpRequestEncoderSpec.getString("/test/no-entity") }
  }

  "An Http request encoder" should "correctly encode a GET request with body " in {
    unixHttpRequestEncoder[HttpResourceEntity[TestContainer]].encode {
      HttpRequestEncoderSpec.get("/test/entity").withResource { TestContainer("string", 128) }
    }.string should be { HttpRequestEncoderSpec.getString("/test/entity", TestContainer("string", 128)) }
  }

  "An Http request encoder" should "correctly encode a POST request without body " in {
    unixHttpRequestEncoder[NoEntity].encode {
      UnixHttpRequest.POST("/test/no-entity").withHeaders { HttpRequestEncoderSpec.headers }
    }.string should be { HttpRequestEncoderSpec.postString("/test/no-entity") }
  }

  "An Http request encoder" should "correctly encode a POST request with body " in {
    unixHttpRequestEncoder[HttpResourceEntity[TestContainer]].encode {
      HttpRequestEncoderSpec.post("/test/entity").withResource { TestContainer("string", 128) }
    }.string should be { HttpRequestEncoderSpec.postString("/test/entity", TestContainer("string", 128)) }
  }

  "An Http request encoder" should "correctly encode a PUT request without body " in {
    unixHttpRequestEncoder[NoEntity].encode {
      UnixHttpRequest.PUT("/test/no-entity").withHeaders { HttpRequestEncoderSpec.headers }
    }.string should be { HttpRequestEncoderSpec.putString("/test/no-entity") }
  }

  "An Http request encoder" should "correctly encode a PUT request with body " in {
    unixHttpRequestEncoder[HttpResourceEntity[TestContainer]].encode {
      HttpRequestEncoderSpec.put("/test/entity").withResource { TestContainer("string", 128) }
    }.string should be { HttpRequestEncoderSpec.putString("/test/entity", TestContainer("string", 128)) }
  }

  "An Http request encoder" should "correctly encode a DELETE request without body " in {
    unixHttpRequestEncoder[NoEntity].encode {
      UnixHttpRequest.DELETE("/test/no-entity").withHeaders { HttpRequestEncoderSpec.headers }
    }.string should be { HttpRequestEncoderSpec.deleteString("/test/no-entity") }
  }

}

object HttpRequestEncoderSpec {

  private val date = ZonedDateTime.now()

  val headers = Set(
    HttpHeader.host,
    HttpHeader.date(date),
    HttpHeader.connection
  )

  def requestString(method: String, path: String) = HttpString.inLine(s"${method.toUpperCase} $path HTTP/1.1", headers)

  def requestString[A](method: String, path: String, resource: A)(implicit E: Encoder[A]) = HttpString.inLine(s"${method.toUpperCase} $path HTTP/1.1", headers, E(resource).noSpaces)

  def request[A](resource: Option[A])(f: UnixHttpRequest.type => UnixHttpRequest[NoEntity]): UnixHttpRequest[Entity] = resource match {
    case Some(a) => f(UnixHttpRequest).withHeaders { headers }.withResource(a)
    case None    => f(UnixHttpRequest).withHeaders { headers }
  }

  def getString(path: String) = requestString("GET", path)

  def getString[A](path: String, resource: A)(implicit E: Encoder[A]) = requestString("GET", path, resource)

  def get[A](path: String, body: Option[A] = None) = request(body) { _.GET(path) }

  def postString(path: String) = requestString("POST", path)

  def postString[A](path: String, resource: A)(implicit E: Encoder[A]) = requestString("POST", path, resource)

  def post[A](path: String, body: Option[A] = None) = request(body) { _.POST(path) }

  def putString(path: String) = requestString("PUT", path)

  def putString[A](path: String, resource: A)(implicit E: Encoder[A]) = requestString("PUT", path, resource)

  def put[A](path: String, body: Option[A] = None) = request(body) { _.PUT(path) }

  def deleteString(path: String) = requestString("DELETE", path)

  def delete[A](path: String, body: Option[A] = None) = request(body) { _.DELETE(path) }

}
