package luzzu.kurrent.json

import io.circe.{ Decoder, Encoder }
import io.circe.Json
import io.circe.syntax._

case class TestContainer(stringValue: String, intValue: Int)

object TestContainer {

  implicit val encoder = Encoder[TestContainer] { container =>
    Json.obj(
      "string" -> container.stringValue.asJson,
      "integer" -> container.intValue.asJson
    )
  }

  implicit val decoder = Decoder[TestContainer] { cursor =>
    for {
      string  <- cursor.downField("string").as[String]
      integer <- cursor.downField("integer").as[Int]
    } yield TestContainer(string, integer)
  }

}
