package luzzu.kurrent.api.http

import cats.effect.IO

import java.time.ZonedDateTime

import io.circe.Encoder

import luzzu.kurrent.api._
import luzzu.kurrent.http._
import luzzu.kurrent.Binary._
import luzzu.kurrent.json.TestContainer
import luzzu.kurrent.{ Entity, NoEntity, UnixResponse }

import org.scalatest.{ FlatSpec, Matchers }

class ResponseSyntaxSpec extends FlatSpec with Matchers {

  "Response Syntax asHttp" should "allow easy transformation from binary without body" in {
    IO.pure { ResponseSyntaxSpec.okBinary }.asHttp[NoEntity].unsafeRunSync should be { ResponseSyntaxSpec.ok }
  }

  "Response Syntax asHttp" should "allow easy transformation from binary with body" in {
    IO.pure { ResponseSyntaxSpec.okBinary(TestContainer("string", 128)) }.asHttp[HttpResourceEntity[TestContainer]].unsafeRunAsync {
      case Left(error)     => throw error
      case Right(response) => response should be { ResponseSyntaxSpec.ok(TestContainer("string", 128)) }
    }
  }

  "Response Syntax asHttpResource" should "allow easy transformation from binary with body" in {
    IO.pure { ResponseSyntaxSpec.okBinary(TestContainer("string", 128)) }.asHttpResource[TestContainer].unsafeRunAsync {
      case Left(error)     => throw error
      case Right(response) => response should be { ResponseSyntaxSpec.ok(TestContainer("string", 128)) }
    }
  }

}

object ResponseSyntaxSpec {

  private val date = ZonedDateTime.now()

  val headers = Set(
    HttpHeader.host,
    HttpHeader.date(date),
    HttpHeader.connection,
    HttpHeader.server("ScalaTest - Kurrent")
  )

  def responseString(statusCode: Int, statusMessage: String) = HttpString.inLine(s"HTTP/1.1 $statusCode $statusMessage", headers)

  def responseString[A](statusCode: Int, statusMessage: String, resource: A)(implicit E: Encoder[A]) = HttpString.inLine(
    s"HTTP/1.1 $statusCode $statusMessage",
    headers,
    E(resource).noSpaces
  )

  def responseHead(statusCode: Int, statusMessage: String) = UnixHttpResponseHead("1.1", statusCode, statusMessage)

  def response[A](statusCode: Int, statusMessage: String, resource: Option[A]): UnixHttpResponse[Entity] = resource match {
    case Some(a) => UnixHttpResponse(statusCode, statusMessage, "1.1", headers, HttpResourceEntity(a))
    case None    => UnixHttpResponse(statusCode, statusMessage, "1.1", headers, NoEntity)
  }

  def okBinary = UnixResponse.binary { responseString(200, "OK").binary }

  def okBinary[A](resource: A)(implicit E: Encoder[A]) = UnixResponse.binary { responseString(200, "OK", resource).binary }

  def ok[A](body: A) = response(200, "OK", Some(body))

  def ok = response(200, "OK", None)

}
