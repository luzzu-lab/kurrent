package luzzu.kurrent.api.http

import java.time.ZonedDateTime

import cats.effect.IO

import org.scalatest.{ BeforeAndAfterAll, FlatSpec, Matchers }

import luzzu.kurrent.Binary._
import luzzu.kurrent.NoEntity
import luzzu.kurrent.connectors.JavaIPCConnector
import luzzu.kurrent.http._
import luzzu.kurrent.api._
import luzzu.kurrent.server.UnixSocketServer

class HttpRequestSpec extends FlatSpec with Matchers with BeforeAndAfterAll {

  private lazy val server = UnixSocketServer.echo()

  override def beforeAll() {
    server.create().start()
  }

  override def afterAll() {
    server.close().unsafeRunSync()
  }

  implicit lazy val connector = JavaIPCConnector.create[IO] { server.path }

  "An Http request" should "be sent correctly over unix sockets" in HttpRequestSpec.get.send[IO].unsafeRunAsync {
    case Left(error)     => throw error
    case Right(response) => response.data.string should be { unixHttpRequestEncoder[NoEntity].encode(HttpRequestSpec.get).string }
  }

}

object HttpRequestSpec {

  private val date = ZonedDateTime.now()

  def get = UnixHttpRequest.GET("/test/path").withHeader { HttpHeader.date(date) }

}
