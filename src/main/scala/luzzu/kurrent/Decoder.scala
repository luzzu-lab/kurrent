package luzzu.kurrent

trait Decoder[A] {

  def decode(binary: Binary): A

}

trait UnixResponseDecoder[A <: UnixResponse] extends Decoder[A]

trait EntityDecoder[A <: Entity] extends Decoder[A]
