package luzzu.kurrent

trait UnixRequest

final case class BinaryUnixRequest(data: Binary) extends UnixRequest

final case class StringUnixRequest(data: String) extends UnixRequest

object UnixRequest {

  def binary(bytes: Binary): UnixRequest = BinaryUnixRequest(bytes)

  def string(string: String): UnixRequest = StringUnixRequest(string)

}

