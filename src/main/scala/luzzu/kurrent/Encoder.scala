package luzzu.kurrent

trait Encoder[A] {

  def encode(a: A): Binary

}

trait UnixRequestEncoder[A <: UnixRequest] extends Encoder[A]

trait EntityEncoder[A <: Entity] extends Encoder[A]