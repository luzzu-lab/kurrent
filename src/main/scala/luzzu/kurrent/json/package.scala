package luzzu.kurrent

import java.nio.charset.Charset

import io.circe.{ Decoder => JsonDecoder, Encoder => JsonEncoder }
import io.circe.jawn.parse

import luzzu.kurrent.Binary._

package object json {

  implicit def binaryJsonEntityEncoder(implicit encoding: Charset = Charset.defaultCharset): EntityEncoder[JsonEntity] = { _.data.noSpaces.binary }

  implicit def binaryJsonEntityDecoder(implicit encoding: Charset = Charset.defaultCharset): EntityDecoder[JsonEntity] = { JsonEntity.parse }

  implicit def binaryJsonDecoder[A](implicit J: JsonDecoder[A], encoding: Charset = Charset.defaultCharset): Decoder[A] = { binary =>
    parse(binary.string).flatMap { J.decodeJson } match {
      case Left(error) => throw error
      case Right(a)    => a
    }
  }

  implicit def binaryJsonEncoder[A](implicit J: JsonEncoder[A], encoding: Charset = Charset.defaultCharset): Encoder[A] = J(_).noSpaces.binary

}
