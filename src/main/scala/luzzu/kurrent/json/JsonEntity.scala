package luzzu.kurrent.json

import java.nio.charset.Charset

import io.circe.Json
import io.circe.jawn.{ parse => circeParse }

import luzzu.kurrent.{ Binary, Entity }
import luzzu.kurrent.Binary._

case class JsonEntity(data: Json) extends Entity {

  type Data = Json

}

object JsonEntity {

  def parse(binary: Binary)(implicit encoding: Charset = Charset.defaultCharset) = circeParse(binary.string) match {
    case Left(error) => throw error
    case Right(json) => JsonEntity(json)
  }

}