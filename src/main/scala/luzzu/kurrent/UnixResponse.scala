package luzzu.kurrent

trait UnixResponse

final case class BinaryUnixResponse(data: Binary) extends UnixResponse

final case class StringUnixResponse(data: String) extends UnixResponse

object UnixResponse {

  def binary(bytes: Binary) = BinaryUnixResponse(bytes)

  def string(string: String) = StringUnixResponse(string)

}