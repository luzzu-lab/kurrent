package luzzu.kurrent.connectors

import java.io.File

import scala.language.higherKinds

import cats.implicits._
import cats.data.Kleisli
import cats.effect.Sync

import jnr.unixsocket._

import luzzu.kurrent.{ Binary, Connector }

class JavaIPCConnector[F[_]](path: String)(implicit F: Sync[F]) extends Connector[F] {

  private def createSocket() = F.delay { UnixSocketChannel.open().socket() }

  private def openConnection(socket: UnixSocket) = F.delay {
    socket.getChannel.connect {
      new UnixSocketAddress(new File(path))
    }
  }

  private def closeConnection(socket: UnixSocket) = F.delay {
    socket.close()
  }

  private def sendRequest(socket: UnixSocket) = Kleisli[F, Binary, Unit] { request =>
    F.delay {
      socket.getOutputStream.write(request)
      socket.getOutputStream.flush()
    }
  }

  private def readResponse(socket: UnixSocket) = F.delay {
    socket.getInputStream.readAllBytes()
  }

  def send = Kleisli[F, Binary, Binary] { request =>
    for {
      socket   <- createSocket()
      _        <- openConnection(socket)
      _        <- sendRequest(socket).run(request)
      response <- readResponse(socket)
      _        <- closeConnection(socket)
    } yield response
  }


}

object JavaIPCConnector {

  def create[F[_]](path: String)(implicit F: Sync[F]): Connector[F] = new JavaIPCConnector[F](path)

}