package luzzu

import scala.language.higherKinds

import cats.data.Kleisli

package object kurrent {

  type Binary = Array[Byte]

  type RawAction[F[_]] = Kleisli[F, Binary, Binary]

  type NoEntity = NoEntity.type

}