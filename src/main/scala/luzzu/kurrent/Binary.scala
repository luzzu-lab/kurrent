package luzzu.kurrent

import java.nio.charset.Charset

object Binary {

  def empty: Binary = Array.empty[Byte]

  def newLine(implicit encoding: Charset = Charset.defaultCharset): Binary = System.lineSeparator.getBytes(encoding)

  implicit class BinaryStringSyntax(s: String)(implicit encoding: Charset = Charset.defaultCharset) {

    def +>>(binary: Binary): Binary = s.getBytes(encoding) ++ binary

    def binary: Binary = s.getBytes(encoding)

  }

  implicit class BinarySyntax(binary: Binary)(implicit encoding: Charset = Charset.defaultCharset) {

    def +|(other: Binary): Binary = binary ++ newLine ++ other

    def string: String = new String(binary, encoding)

  }

}
