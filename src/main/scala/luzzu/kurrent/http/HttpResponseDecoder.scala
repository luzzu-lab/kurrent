package luzzu.kurrent.http

import java.nio.charset.Charset

import scala.util.{Failure, Success, Try}

import luzzu.kurrent.{ Binary, Entity, EntityDecoder, NoEntity, UnixResponseDecoder }
import Binary._

class HttpResponseDecoder[A <: Entity](implicit D: EntityDecoder[A], encoding: Charset = Charset.defaultCharset) extends UnixResponseDecoder[UnixHttpResponse[A]] {

  private val headRegex = "HTTP\\/(\\d\\.\\d) (\\d{3}) ([\\w,\\s]*)".r

  private def _parse1(strings: List[String]) = strings match {
    case headRegex(httpVersion, statusCode, statusMessage) :: tail => Success { UnixHttpResponseHead(httpVersion, statusCode.toInt, statusMessage) -> tail }
    case head :: _                                                 => Failure { new RuntimeException(s"Unable to parse HTTP head [$head]") }
    case Nil                                                       => Failure { new RuntimeException(s"Attempted to parse an empty response") }
  }

  private def _parse2(strings: List[String], headers: Set[HttpHeader] = HttpHeaders.empty): Try[(Set[HttpHeader], List[String])] = strings match {
    case (name :> value) :: tail => _parse2(tail, headers + HttpHeader(name, value))
    case "" :: tail              => _parse2(tail, headers)
    case other                   => Success { headers -> other }
  }

  private def _parse3(strings: List[String])(implicit encoding: Charset = Charset.defaultCharset) = Try {
    D.decode {
      strings.filter { _.nonEmpty }.mkString.getBytes(encoding)
    }
  }

  private def _parse(strings: List[String])(implicit encoding: Charset = Charset.defaultCharset) = for {
    (head, part1)        <- _parse1(strings)
    (httpHeaders, part2) <- _parse2(part1)
    entity               <- _parse3(part2)
  } yield UnixHttpResponse(
    status  = HttpResponseStatus(head.statusCode, head.message),
    headers = httpHeaders,
    entity  = entity,
    version = head.httpVersion
  )

  def decode(binary: Binary): UnixHttpResponse[A] = _parse { binary.string.split("\\r?\\n").toList } match {
    case Success(response) => response
    case Failure(error)    => throw new RuntimeException(s"Unable to parse HTTP response: [${error.getMessage}]", error)
  }

}
