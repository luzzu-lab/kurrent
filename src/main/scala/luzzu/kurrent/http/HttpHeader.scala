package luzzu.kurrent.http

import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter

import cats.Show
import cats.syntax.show.toShow

sealed case class HttpHeader(name: String, value: String)

object HttpHeader {

  implicit val headerShow: Show[HttpHeader] = {
    case HttpHeader(name, value) => s"$name: $value"
  }

  implicit val dateTimeShow: Show[ZonedDateTime] = DateTimeFormatter.RFC_1123_DATE_TIME.format(_)

  def apply(string: String): Option[HttpHeader] = string match {
    case name :> value => Some { HttpHeader(name, value) }
    case _             => None
  }

  def custom[A: Show](name: String, value: A)    = apply(name, value.show)

  def host(value: String): HttpHeader            = apply(HttpHeaderNames.host,  value)

  def host: HttpHeader                           = host("localhost")

  def age(value: Int)                            = apply(HttpHeaderNames.age, value.toString)

  def connection(value: String): HttpHeader      = apply(HttpHeaderNames.connection, value)

  def connection: HttpHeader                     = connection("close")

  def contentDisposition(value: String)          = apply(HttpHeaderNames.contentDisposition, value)

  def contentEncoding(value: String)             = apply(HttpHeaderNames.contentEncoding, value)

  def contentLength(value: Long)                 = apply(HttpHeaderNames.contentLength, value.toString)

  def contentType(value: String)                 = apply(HttpHeaderNames.contentType, value)

  def date(value: ZonedDateTime)                 = apply(HttpHeaderNames.date, value.show)

  def date: HttpHeader                           = apply(HttpHeaderNames.date, ZonedDateTime.now().show)

  def location(value: String)                    = apply(HttpHeaderNames.location, value)

  def server(value: String)                      = apply(HttpHeaderNames.server, value)

  def transferEncoding(value: String)            = apply(HttpHeaderNames.transferEncoding, value)

  def wwwAuthenticate(value: String): HttpHeader = apply(HttpHeaderNames.wwwAuthenticate, value)

  def wwwAuthenticate: HttpHeader                = wwwAuthenticate("Basic")

}

object HttpHeaderNames {

  val host               = "Host"

  val age                = "Age"

  val connection         = "Connection"

  val contentDisposition = "Content-Disposition"

  val contentEncoding    = "Content-Encoding"

  val contentLength      = "Content-Length"

  val contentType        = "Content-Type"

  val date               = "Date"

  val location           = "Location"

  val server             = "Server"

  val transferEncoding   = "Transfer-Encoding"

  val wwwAuthenticate    = "WWW-Authenticate"

}

object HttpHeaders {

  def default = Set(
    HttpHeader.host,
    HttpHeader.date,
    HttpHeader.connection
  )

  def empty = Set.empty[HttpHeader]

}

object :> {

  private[http] val headerRegex = "([\\w,\\d,\\-,_]+)\\:\\s?(.*)".r

  def unapply(arg: HttpHeader): Option[(String, String)] = Some { arg.name -> arg.value }

  def unapply(arg: String): Option[(String, String)] = arg match {
    case headerRegex(name, value) => Some { name -> value }
    case _                        => None
  }


}