package luzzu.kurrent.http

import luzzu.kurrent.Entity

case class HttpResourceEntity[A](data: A) extends Entity {

  type Data = A

}