package luzzu.kurrent.http

import java.nio.charset.Charset

import cats.syntax.show.toShow

import luzzu.kurrent._
import Binary._
import HttpQueryParam.queryShow

class HttpRequestEncoder[A <: Entity](implicit E: EntityEncoder[A], encoding: Charset = Charset.defaultCharset) extends UnixRequestEncoder[UnixHttpRequest[A]] {

  val eof = "\n\r".binary

  private def query(data: UnixHttpRequest[A]) = if (data.query.nonEmpty) data.query.map { _.show }.mkString("?", "&", "") else ""

  private def head(data: UnixHttpRequest[A]) = s"${data.method.show} ${data.path}${query(data)} HTTP/${data.version}" +: data.headers.map { _.show }.toSeq

  def body(data: UnixHttpRequest[A]) = data.entity match {
    case Some(entity) => E.encode(entity)
    case None         => Binary.empty
  }

  def encode(data: UnixHttpRequest[A]) = { head(data) mkString "\n" }.binary +| body(data) +| eof

}