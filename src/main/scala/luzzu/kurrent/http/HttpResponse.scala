package luzzu.kurrent.http

import luzzu.kurrent.{ Entity, NoEntity, UnixResponse }

case class UnixHttpResponse[+A <: Entity](status: HttpResponseStatus,
                                          version: String = "1.1",
                                          headers: Set[HttpHeader] = HttpHeaders.empty,
                                          entity: A = NoEntity) extends UnixResponse

object UnixHttpResponse {

  def apply[A <: Entity](statusCode: Int,
                         statusMessage: String,
                         version: String,
                         headers: Set[HttpHeader],
                         entity: A): UnixHttpResponse[A] = apply(
    HttpResponseStatus(statusCode, statusMessage),
    version,
    headers,
    entity
  )

}

private[http] case class HttpResponseStatus(code: Int, message: String)

sealed case class UnixHttpResponseHead(httpVersion: String, statusCode: Int, message: String)