package luzzu.kurrent.http

import java.nio.charset.Charset

import luzzu.kurrent.{Binary, Decoder, EntityDecoder}

class HttpResourceEntityDecoder[A](implicit D: Decoder[A], encoding: Charset = Charset.defaultCharset) extends EntityDecoder[HttpResourceEntity[A]] {

  def decode(binary: Binary) = HttpResourceEntity { D.decode(binary) }

}
