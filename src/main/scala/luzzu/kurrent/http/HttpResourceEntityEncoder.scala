package luzzu.kurrent.http

import java.nio.charset.Charset

import luzzu.kurrent.{ Encoder, EntityEncoder }

class HttpResourceEntityEncoder[A](implicit E: Encoder[A], encoding: Charset = Charset.defaultCharset) extends EntityEncoder[HttpResourceEntity[A]] {

  def encode(entity: HttpResourceEntity[A]) = E.encode { entity.data }

}
