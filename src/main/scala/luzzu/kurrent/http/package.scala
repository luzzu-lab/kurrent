package luzzu.kurrent

import cats.Show

package object http {

  type BinaryUnixHttpResponse = UnixHttpResponse[BinaryEntity]

  implicit def httpMethodShow: Show[HttpMethod] = {
    case Get    => "GET"
    case Post   => "POST"
    case Put    => "PUT"
    case Delete => "DELETE"
  }

}
