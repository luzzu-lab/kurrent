package luzzu.kurrent.http

import luzzu.kurrent.{ NoEntity, UnixRequest, Entity }

case class UnixHttpRequest[+A <: Entity](path: String,
                                        method: HttpMethod,
                                        version: String = "1.1",
                                        headers: Set[HttpHeader] = HttpHeaders.default,
                                        query: Set[HttpQueryParam] = HttpQueryParams.empty,
                                        entity: Option[A] = None) extends UnixRequest {

  private def addHeader(newHeader: HttpHeader, oldHeaders: Set[HttpHeader] = headers) = oldHeaders.find { _.name == newHeader.name } match {
    case Some(oldHeader) => headers - oldHeader + newHeader
    case None            => headers + newHeader
  }

  private def addHeaders(newHeaders: Set[HttpHeader]) = newHeaders.foldLeft(headers) { (newSet, newHeader) => addHeader(newHeader, newSet) }

  def withEntity[B <: Entity](b: B) = this.copy[B](entity = Some(b))

  def withResource[B](b: B) = withEntity { HttpResourceEntity(b) }

  def withHeader(httpHeader: HttpHeader) = this.copy(headers = addHeader(httpHeader))

  def withHeaders(httpHeaders: Set[HttpHeader]) = this.copy(headers = httpHeaders)

  def ?(httpQueryParam: HttpQueryParam) = this.copy(query = this.query + httpQueryParam)

  def ?(httpQueryParams: Set[HttpQueryParam]) = this.copy(query = this.query ++ httpQueryParams)

}

object UnixHttpRequest {

  def GET(path: String, version: String = "1.1")    = apply[NoEntity](path, Get, version)

  def POST(path: String, version: String = "1.1")   = apply[NoEntity](path, Post, version)

  def PUT(path: String, version: String = "1.1")    = apply[NoEntity](path, Put, version)

  def DELETE(path: String, version: String = "1.1") = apply[NoEntity](path, Delete, version)

}

sealed trait HttpMethod

case object Get extends HttpMethod

case object Post extends HttpMethod

case object Put extends HttpMethod

case object Delete extends HttpMethod
