package luzzu.kurrent.http

import java.net.{ URLEncoder, URLDecoder }
import java.nio.charset.Charset

import cats.Show
import cats.syntax.show.toShow

sealed case class HttpQueryParam(name: String, value: String)

object HttpQueryParam {

  implicit def queryShow(implicit encoding: Charset = Charset.defaultCharset): Show[HttpQueryParam] = {
    case HttpQueryParam(name, value) => s"$name=${URLEncoder.encode(value, encoding)}"
  }

  def apply(string: String)(implicit encoding: Charset = Charset.defaultCharset): Option[HttpQueryParam] = string match {
    case name := value => Some { HttpQueryParam(name, value) }
    case _             => None
  }

  def apply[A: Show](name: String, value: A): HttpQueryParam = apply(name, value.show)

}

object HttpQueryParams {

  def empty = Set.empty[HttpQueryParam]

  implicit class HttpQuerySyntax(name: String) {

    def :=(value: String)(implicit encoding: Charset = Charset.defaultCharset) = HttpQueryParam(name, value)

  }

}

object := {

  private[http] val headerRegex = "([\\w,\\d,\\-,_]+)\\=(.*)".r

  def unapply(arg: String)(implicit encoding: Charset = Charset.defaultCharset): Option[(String, String)] = arg match {
    case headerRegex(name, value) => Some { name -> URLDecoder.decode(value, encoding) }
    case _                        => None
  }


}