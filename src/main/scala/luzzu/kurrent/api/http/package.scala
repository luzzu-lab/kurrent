package luzzu.kurrent.api

import java.nio.charset.Charset

import scala.language.higherKinds

import cats.Functor

import luzzu.kurrent._
import luzzu.kurrent.http._

package object http {

  implicit def binaryHttpResponseMap[A <: Entity](implicit D: EntityDecoder[A], encoding: Charset = Charset.defaultCharset): (BinaryUnixResponse ~~> UnixHttpResponse[A]) = { binary =>
    unixHttpResponseDecoder[A].decode { binary.data }
  }

  implicit def unixHttpRequestEncoder[A <: Entity](implicit E: EntityEncoder[A], encoding: Charset = Charset.defaultCharset): UnixRequestEncoder[UnixHttpRequest[A]] =
    new HttpRequestEncoder[A]

  implicit def unixHttpResponseDecoder[A <: Entity](implicit D: EntityDecoder[A], encoding: Charset = Charset.defaultCharset): UnixResponseDecoder[UnixHttpResponse[A]] =
    new HttpResponseDecoder[A]

  implicit def httpResourceEntityEncoder[A](implicit E: Encoder[A], encoding: Charset = Charset.defaultCharset): EntityEncoder[HttpResourceEntity[A]] =
    new HttpResourceEntityEncoder[A]

  implicit def httpResourceEntityDecoder[A](implicit D: Decoder[A], encoding: Charset = Charset.defaultCharset): EntityDecoder[HttpResourceEntity[A]] =
    new HttpResourceEntityDecoder[A]

  implicit class ResponseSyntax[F[_]](response: F[BinaryUnixResponse]) {

    def asHttp[A <: Entity](implicit D: EntityDecoder[A], F: Functor[F], encoding: Charset = Charset.defaultCharset) = F.map(response) { binaryHttpResponseMap[A] }

    def asHttpResource[A](implicit D: Decoder[A], F: Functor[F], encoding: Charset = Charset.defaultCharset) = asHttp[HttpResourceEntity[A]]

  }

}
