package luzzu.kurrent

import java.nio.charset.Charset

import scala.language.higherKinds

import cats.Functor
import cats.syntax.functor.toFunctorOps

import luzzu.kurrent.Binary._

package object api {

  type ~~>[A <: UnixResponse, B <: UnixResponse] = A => B

  // Encoders

  implicit val binaryEncoder: EntityEncoder[BinaryEntity] = { _.data }

  implicit def stringEncoder(implicit encoding: Charset = Charset.defaultCharset): EntityEncoder[StringEntity] = { _.binary }

  implicit val noEntityEncoder: EntityEncoder[NoEntity] = { _ => Binary.empty }

  // Decoders

  implicit val binaryDecoder: EntityDecoder[BinaryEntity] = BinaryEntity(_)

  implicit def stringDecoder(implicit encoding: Charset = Charset.defaultCharset): EntityDecoder[StringEntity] = { binary => StringEntity(binary.string) }

  implicit val noEntityDecoder: EntityDecoder[NoEntity] = { _ => NoEntity }

  // Response Mappings

  implicit def identityResponseMap[A <: UnixResponse]: (A ~~> A) = identity[A]

  implicit def binaryStringResponseMap(implicit encoding: Charset = Charset.defaultCharset): (BinaryUnixResponse ~~> StringUnixResponse) = { binary =>
    UnixResponse.string(binary.data.string)
  }

  implicit def binaryDecodedResponseMap[A <: UnixResponse](implicit D: UnixResponseDecoder[A]): (BinaryUnixResponse ~~> A) = { binary =>
    D.decode(binary.data)
  }

  // Syntax

  implicit class RequestSyntax[A <: UnixRequest](request: A) {

    def send[F[_]](implicit C: Connector[F], F: Functor[F], E: UnixRequestEncoder[A]) = C.send.run { E.encode(request) }.map { UnixResponse.binary }

  }

  implicit class ResponseSyntax[F[_], A <: UnixResponse](response: F[A]) {

    def as[B <: UnixResponse](implicit M: A ~~> B, F: Functor[F]) = F.map(response) { M }

  }

}
