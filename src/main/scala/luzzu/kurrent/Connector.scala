package luzzu.kurrent

import scala.language.higherKinds

trait Connector[F[_]] {

  def send: RawAction[F]

}
