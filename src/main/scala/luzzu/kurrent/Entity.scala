package luzzu.kurrent

import java.nio.charset.Charset

trait Entity {

  type Data

  def data: Data

}

case class StringEntity(data: String) extends Entity {

  type Data = String

  def binary(implicit encoding: Charset = Charset.defaultCharset) = data.getBytes(encoding)

}

case class BinaryEntity(data: Binary) extends Entity {

  type Data = Binary

  def string(implicit encoding: Charset = Charset.defaultCharset) = new String(data, encoding)

}

case object NoEntity extends Entity {

  type Data = Nothing

  final def data = throw new RuntimeException("Cannot access data for NoEntity")

}