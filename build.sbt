import sbt._

organization := "luzzu"

name         := "kurrent"

scalaVersion := Library.scalaVersion

javacOptions ++= Seq("-source", "1.8", "-target", "1.8", "-Xlint")

resolvers           += Opts.resolver.sonatypeReleases

libraryDependencies += Library.dependencies.jnr

libraryDependencies += Library.dependencies.circeCore

libraryDependencies += Library.dependencies.circeJawn

libraryDependencies += Library.dependencies.catsCore

libraryDependencies += Library.dependencies.catsEffect

libraryDependencies += Library.dependencies.scalactic

libraryDependencies += Library.dependencies.scalaTest

testOptions in Test += Tests.Filter { _ endsWith "Spec" }

coverageMinimum := 70

coverageFailOnMinimum := false

coverageOutputXML := true

lazy val root = Project(id = "kurrent", base = Library.base)


