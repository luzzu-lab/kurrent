kurrent - WIP
=======

[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)

Kurrent is a *work-in-progress* purely functional scala wrapper for Unix Domain socket communication. It abstracts the communication protocol to promote flexibility. It is intended to ship with HTTP support out of the box, with integration via circe to allow for Json un/marshalling.

More documentation to follow when APIs are made stable, and coverage is increased.
