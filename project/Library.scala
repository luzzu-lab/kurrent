import sbt._

object Library {

  def base: File = file(".")

  val scalaVersion = "2.12.1"

  lazy val dependencies = Dependencies

}