import sbt._

object Dependencies {

  val jnr        = "com.github.jnr"          % "jnr-unixsocket" % Versions.jnr

  val circeCore  = "io.circe"               %% "circe-core"     % Versions.circe

  val circeJawn  = "io.circe"               %% "circe-jawn"     % Versions.circe

  val catsCore   = "org.typelevel"          %% "cats-core"      % Versions.cats

  val catsEffect = "org.typelevel"          %% "cats-effect"    % Versions.catsEffect

  val scalactic  = "org.scalactic"          %% "scalactic"      % Versions.scalactic

  val scalaTest  = "org.scalatest" %% "scalatest" % Versions.scalaTest % Test

}