object Versions {

  val jnr        = "0.18"

  val cats       = "1.1.0"

  val catsEffect = "1.0.0-RC"

  val circe      = "0.9.3"

  val scalactic  = "3.0.5"

  val scalaTest  = "3.0.5"

}